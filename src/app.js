const express = require('express')
const app = express()
require("mongoose");
require('dotenv').config()
const coneccion = require('./database/database');
const AuthToken = require('./router/autentificacion');


const port = process.env.PORT || 3000

app.use(express.json());

app.use(require('./router/login'));

app.use(AuthToken);

app.use(require('./router/usuario'));
app.use(require('./router/tipo'));
app.use(require('./router/bitacora'));
app.use(require('./router/entrada_salida'));




app.listen(port , ()=> console.log('inicio con port : ' + port))