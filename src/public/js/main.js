function obtenerTipo() {

 
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://localhost:3000/tipo", requestOptions)
        .then(response => response.text())
        .then(result => {
            let datos = JSON.parse(result)
            console.log(datos);
            
            const table = document.getElementById("tipo");
            
            datos.forEach((e,i) => {  
            
              let tr = document.createElement("tr"); 
              
              let td = document.createElement("td");
              td.classList.add("index");
              td.innerHTML = i+1;
              tr.appendChild(td); 
            
              for (p in e) {  
            
                let td = document.createElement("td"); 
                td.classList.add(p);
                td.innerHTML = e[p]; 
            
                tr.appendChild(td); 
            
              }
            
              table.appendChild(tr); 
            
            });   

        })
        .catch(error => console.log('error', error));
}



function obtenerBitacora() {

 
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://localhost:3000/bitacora", requestOptions)
        .then(response => response.text())
        .then(result => {
            let datos = JSON.parse(result)
            console.log(datos);
            
            const table = document.getElementById("bitacora");
            
            datos.forEach((e,i) => {  
            
              let tr = document.createElement("tr"); 
              
              let td = document.createElement("td");
              td.classList.add("index");
              td.innerHTML = i+1;
              tr.appendChild(td); 
            
              for (p in e) {  
            
                let td = document.createElement("td"); 
                td.classList.add(p);
                td.innerHTML = e[p]; 
            
                tr.appendChild(td); 
            
              }
            
              table.appendChild(tr); 
            
            });   

        })
        .catch(error => console.log('error', error));
}



function obtenerUsuario() {

 
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://localhost:3000/usuario", requestOptions)
        .then(response => response.text())
        .then(result => {
            let datos = JSON.parse(result)
            console.log(datos);
            
            const table = document.getElementById("usuario");
            
            datos.forEach((e,i) => {  
            
              let tr = document.createElement("tr"); 
              
              let td = document.createElement("td");
              td.classList.add("index");
              td.innerHTML = i+1;
              tr.appendChild(td); 
            
              for (p in e) {  
            
                let td = document.createElement("td"); 
                td.classList.add(p);
                td.innerHTML = e[p]; 
            
                tr.appendChild(td); 
            
              }
            
              table.appendChild(tr); 
            
            });   

        })
        .catch(error => console.log('error', error));
}


function obtenerenSa() {

 
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://localhost:3000/entrada_salida", requestOptions)
        .then(response => response.text())
        .then(result => {
            let datos = JSON.parse(result)
            console.log(datos);
            
            const table = document.getElementById("enSa");
            
            datos.forEach((e,i) => {  
            
              let tr = document.createElement("tr"); 
              
              let td = document.createElement("td");
              td.classList.add("index");
              td.innerHTML = i+1;
              tr.appendChild(td); 
            
              for (p in e) {  
            
                let td = document.createElement("td"); 
                td.classList.add(p);
                td.innerHTML = e[p]; 
            
                tr.appendChild(td); 
            
              }
            
              table.appendChild(tr); 
            
            });   

        })
        .catch(error => console.log('error', error));
}