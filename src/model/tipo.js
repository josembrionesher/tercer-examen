const mongoose = require('mongoose')    
const tipo = mongoose.Schema({
    idusuario : {
        type : Number,
        unique:true
    },
    descripcion : {
        type : String,
        unique:true
    }
})
module.exports =  mongoose.model( 'tipo' , tipo)