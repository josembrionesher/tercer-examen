const mongoose = require('mongoose')    
const entrada_salida = mongoose.Schema({
    idusuario : {
        type : String,
    },
    fechaEntrada : {
        type : Date,
    },
    horaSalida : {
        type : Date,
    }
})
module.exports =  mongoose.model( 'entrada_salida' , entrada_salida)