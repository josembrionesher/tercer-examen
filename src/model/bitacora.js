const mongoose = require('mongoose')    
const bitacora = mongoose.Schema({
    idusuario : {
        type : String,
    },
    actividad : {
        type : String,
    },
    fechaActividad : {
        type : Date,
    }
})
module.exports =  mongoose.model( 'bitacora' , bitacora)