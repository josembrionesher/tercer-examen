var jwt = require('jsonwebtoken');
module.exports = function (req, res, next) {
    if (req.headers.authorization) {
        
        try {
            const tipoa = req.headers.authorization.split(' ')[1];
            const des=jwt.verify(tipoa,'palabra clave')  

            const tipou = des.tipo
            console.log(tipou);
           
            if(req.method == 'GET' && tipou == (1||2||3||4)){ //todos pueden ver
                return next();
            }

            if (req.method == 'POST' && ("/entrada_salida" || "/bitacora")) { //todos (maquina)
                return next()
            } else 
            if(req.method == 'DELETE' || req.method == 'PUT'){
                if (tipou == (1||2)) { //cliente o administrador
                    return next();    
                } else {
                    return res.json({message:"no autorizado4"});    
                }
                
            }else{
                return res.json({message:"no autorizado 3"});
            }
        } catch (error) {
            return res.json({
                titulo:"Error en el token",
                message:error});    
        }
    } else {
        return res.json({message:"no autorizado2"});
    }    
}