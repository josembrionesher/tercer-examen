const router = require('express').Router()
const entrada_salidaSchema = require('../model/entrada_salida');
const cors = require('cors');
router.use(cors());

router.get('/entrada_salida' ,async (req , res)=>{
    await entrada_salidaSchema.find().exec().then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })
});

router.post('/entrada_salida' , async (req , res)=>{
    const datos = await entrada_salidaSchema(req.body);
    datos.save().then((data) =>{
        res.json({mensaje:"entrada_salida registrada"});
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })

})


router.delete("/entrada_salida/:id", async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) { return res.json({ mesaje: "Falta id" });  }
      const eliminado = await entrada_salidaSchema.findByIdAndDelete(id);
      res.json({ mensaje: "Eliminado" });
    } catch (error) {
      console.log(error);
    }
  });
  
router.put('/entrada_salida/:id' , async (req , res)=>{
    const {id} = await req.params;

    const datos = await entrada_salidaSchema(req.body);

    entrada_salidaSchema.findByIdAndUpdate(id, {$set:{
        idusuario : datos.idusuario ,
        fechaEntrada:datos.fechaEntrada,
        fechaSalida:datos.fechaSalida

    }} ).then((data)=>{
        res.json({mensaje:"entrada_salida actualizado"})
    }).catch((error)=>{
        res.json({mensaje:error})
    })
})


module.exports  = router