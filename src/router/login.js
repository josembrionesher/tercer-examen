const router = require('express').Router()
const usuarioSchema = require('../model/usuario');
var jwt = require('jsonwebtoken');
require('dotenv').config()
const cors = require('cors');

router.use(cors());

router.post('/login' , (req , res)=>{
    let usuarioo = req.body.usuarioo;
    let password = req.body.password;
    usuarioSchema.findOne({usuarioo:usuarioo}).then(user => {
        if(!user){return res.json({message: 'el usuario no existe'})
    } else{
        const comparacion = user.password
        if(password === comparacion){
            precarga = {
                nombre : user.nombre,
                usuarioo : user.usuarioo,
                correo :user.correo,
                password :user.password,
                tipo : user.tipo 
            }
            try {
                const encri= jwt.sign(precarga, 'palabra clave');
                return res.json({token: encri})   
            } catch (error) {
                return res.json({message: error});
            }
            
        }else{
            return res.json({message: 'error en contraseña'});
        }
    }
    }).catch(error => {
        console.log(error);
        res.status(500).send({error});
    });
})



module.exports  = router