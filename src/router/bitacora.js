const router = require('express').Router()
const bitacoraSchema = require('../model/bitacora');
const cors = require('cors');
router.use(cors());

router.get('/bitacora' ,async (req , res)=>{
    await bitacoraSchema.find().exec().then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })
});

router.post('/bitacora' , async (req , res)=>{

    const datos = await bitacoraSchema(req.body);
        datos.save().then((data) =>{
        res.json({mensaje:"bitacora registrada"});
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })

})


router.delete("/bitacora/:id", async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) { return res.json({ mesaje: "Falta id" });  }
      const eliminado = await bitacoraSchema.findByIdAndDelete(id);
      res.json({ mensaje: "Eliminado" });
    } catch (error) {
      console.log(error);
    }
  });
  
router.put('/bitacora/:id' , async (req , res)=>{
    const {id} = await req.params;

    const datos = await bitacoraSchema(req.body);

    bitacoraSchema.findByIdAndUpdate(id, {$set:{
        idusuario : datos.idusuario ,
        actividad : datos.actividad,
        fechaActividad:datos.fechaActividad
    }} ).then((data)=>{
        res.json({mensaje:"bitacora actualizado"})
    }).catch((error)=>{
        res.json({mensaje:error})
    })
})


module.exports  = router