const router = require('express').Router()
const usuarioSchema = require('../model/usuario');
const cors = require('cors');
router.use(cors());

router.get('/usuario' ,async (req , res)=>{
    await usuarioSchema.find().exec().then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })
});

router.post('/usuario' , async (req , res)=>{
    const datos = await usuarioSchema(req.body);
    datos.save().then((data) =>{
        res.json({mensaje:"usuario registrado"});
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })

})


router.delete("/usuario/:id", async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) { return res.json({ mesaje: "Falta id" });  }
      const eliminado = await usuarioSchema.findByIdAndDelete(id);
      res.json({ mensaje: "Eliminado" });
    } catch (error) {
      console.log(error);
    }
  });
  
router.put('/usuario/:id' , async (req , res)=>{
    const {id} = await req.params;

    const datos = await usuarioSchema(req.body);

    usuarioSchema.findByIdAndUpdate(id, {$set:{
        nombre : datos.nombre,
        usuarioo : datos.usuarioo,
        correo :datos.correo,
        password :datos.password,
        tipo : datos.tipo   
    }} ).then((data)=>{
        res.json({mensaje:"Usuario actualizado"})
    }).catch((error)=>{
        res.json({mensaje:error})
    })
})


module.exports  = router