const router = require('express').Router()
const tipoSchema = require('../model/tipo');
const cors = require('cors');
router.use(cors());

router.get('/tipo' ,async (req , res)=>{
    await tipoSchema.find().exec().then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.json({mensaje:"error"});
    })
});

router.post('/tipo' , async (req , res)=>{
    const datos = await tipoSchema(req.body);
    datos.save().then((data) =>{
        res.json({mensaje:"tipo registrado"});
    }).catch((error)=>{
        res.json({mensaje:"error"});
        console.log(error);
    })

})


router.delete("/tipo/:id", async (req, res) => {
    try {
      const { id } = req.params;
      if (!id) { return res.json({ mesaje: "Falta id" });  }
      const eliminado = await tipoSchema.findByIdAndDelete(id);
      res.json({ mensaje: "Eliminado" });
    } catch (error) {
      console.log(error);
    }
  });
  
router.put('/tipo/:id' , async (req , res)=>{
    const {id} = await req.params;

    const datos = await tipoSchema(req.body);

    tipoSchema.findByIdAndUpdate(id, {$set:{
        idusuario : datos.idusuario ,
        descripcion : datos.descripcion
    }} ).then((data)=>{
        res.json({mensaje:"tipo actualizado"})
    }).catch((error)=>{
        res.json({mensaje:error})
    })
})


module.exports  = router